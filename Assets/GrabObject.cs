﻿using UnityEngine;
using System.Collections;
public class GrabObject : MonoBehaviour
{
    private SteamVR_TrackedObject trackedObject;
    private SteamVR_Controller.Device device;
    public float throwSpeed = 2.0f;

    void Start()
    {
        trackedObject = GetComponent<SteamVR_TrackedObject>();
    }

    void Update()
    {
        device = SteamVR_Controller.Input((int)trackedObject.index);
    }

    void OnTriggerStay(Collider col)
    {
        // Track if it is tagged as a ball. (Unity tag for a GameObject)
        if (col.tag == "Ball")
        {
            // If the trigger button is pressed
            if (device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
            {
                // Freeze the Ball when it is grabbed by the controller
                col.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                // Set the Ball to follow the controller
                col.gameObject.transform.SetParent(gameObject.transform);
            }
            // When trigger button is released
            if (device.GetPressUp(SteamVR_Controller.ButtonMask.Trigger))
            {
                // Free the Ball to Physics Engine
                col.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                // Set the Ball to unfollow the controller
                col.gameObject.transform.SetParent(null);
                // Provide an initial to the Ball to throw it away
                TossObject(col.attachedRigidbody);
            }
        }
    }
    public void TossObject(Rigidbody rigidBody)
    {
        // Provide a vertical speed
        rigidBody.velocity = device.velocity * throwSpeed;
        // Provide an angular speed
        rigidBody.angularVelocity = device.angularVelocity;
    }
}